#!/bin/bash -ex

dir_artifacts="artifacts"

function curl_download(){
    local remote_url="${1}"
    local local_path="${2}"
    local header="${3}"
    if [ -n "${header}" ]; then
        curl -fsSL --header "${header}" "${remote_url}" -o "${local_path}"
    else
        curl -fsSL "${remote_url}" -o "${local_path}"
    fi
}
url_common_funcs="https://gitlab.com/Linaro/ltsk/ltsk-tuxtrigger/-/raw/main/scripts/ltsk-common-bash-function.sh"
f_common_funcs="scripts/ltsk-common-bash-function.sh"
curl_download "${url_common_funcs}" "${f_common_funcs}"
# shellcheck source=/dev/null
source "${f_common_funcs}"

f_report="${dir_artifacts}/report.txt"
rm -fr "${f_report}"
builds_total=0
builds_passed=0
for f_tuxsuite_ouput_json in "${dir_artifacts}"/tuxsuite-aosp-output-lkft-*.json; do
    build_name=$(jq -r '.name' "${f_tuxsuite_ouput_json}")
    build_download_url=$(jq -r '.download_url' "${f_tuxsuite_ouput_json}")
    f_metadata_url="${build_download_url}/metadata.json"
    f_metadata_local="${dir_artifacts}/tuxsuite-metadata-${build_name}.json"
    if [ "X${DEV_PRIVATE_GIT}" = "XTrue" ]; then
        # When DEV_PRIVATE_GIT is true, it means to use the dev-private-git.l.o
        # repository, and the tuxsuite will be done in private mode
        # and then it would need to pass the headers to access the metadata.json
        header="Authorization: ${TUXSUITE_TOKEN}"
        curl_download "${f_metadata_url}" "${f_metadata_local}" "${header}"
    else
        curl_download "${f_metadata_url}" "${f_metadata_local}"
    fi
    build_result=$(jq -r '.results.status' "${f_metadata_local}")

    # shellcheck disable=SC2129
    echo ""                                 >> "${f_report}"
    echo "${build_name}: ${build_result}"   >> "${f_report}"
    echo "    ${build_download_url}"        >> "${f_report}"

    builds_total=$((builds_total + 1))
    if [ "X${build_result}" = "Xpass" ]; then
        builds_passed=$((builds_passed + 1))
    fi

done

# SUBJECT_KEYWORD will be defined in the project level
# shellcheck disable=SC2153
if [ -n "${SUBJECT_KEYWORD}" ]; then
    subject_keyword="${SUBJECT_KEYWORD}"
else
    subject_keyword="lkft-aosp"
fi
if [ -n "${CI_PIPELINE_ID}" ]; then
    msg_subject="[${subject_keyword}] [ ${builds_passed} / ${builds_total} ] ${CI_PIPELINE_ID}: All AOSP builds have been finished"
else
    msg_subject="[${subject_keyword}] [ ${builds_passed} / ${builds_total} ] All AOSP builds have been finished"
fi
send_mail_common "${f_report}" "${msg_subject}"
