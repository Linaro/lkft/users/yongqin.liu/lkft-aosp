#!/bin/bash -ex

name_android_build_config="${1}"

if [ -z "${name_android_build_config}" ]; then
    echo "No android build configuration file specified"
    exit
fi

dir_artifacts="artifacts"
if [ -n "${LOCAL_PREFIX_CONFIGS}" ]; then
    url_prefix_configs="${LOCAL_PREFIX_CONFIGS}"
else
    url_prefix_configs="https://android-git.linaro.org/android-build-configs.git/plain/"
fi

############ COMMON FUNCTIONS ##########################
function func_curl(){
    local url_remote="${1}"
    local path_local="${2}"

    if echo "${url_remote}"|grep -q '^/'; then
        # copy local files without check the md5sum
        if [ -z "${path_local}" ]; then
            cp -vf "${url_remote}" "./"
        else
            cp -vf "${url_remote}" "${path_local}"
        fi
        return $?
     else
        curl -fsSL "${url_remote}" -o "${path_local}"
     fi
}

function generate_tuxsuite_aosp_configuration(){
    local f_tuxsuite_aosp="${1}"
    local f_android_build_config="${2}"

    # shellcheck disable=SC2155
    local f_android_build_config_name=$(basename "${f_android_build_config}")

    # shellcheck disable=SC1090
    source "${f_android_build_config}"

cat >"${f_tuxsuite_aosp}" <<__EOF__
{
    "artifacts": [
__EOF__

    if echo "${TARGET_PRODUCT}" | grep "aosp_arm64"; then
        # for cts, vts, gsi
        DIR_PUB_SRC_PRODUCT="out/target/product/generic_arm64"
    elif echo "${TARGET_PRODUCT}" | grep "trunk_staging"; then
        # with the main branch, there are release information, like aosp_arm64-trunk_staging
        name_product=$(echo "${TARGET_PRODUCT}"|cut -d- -f1)
        DIR_PUB_SRC_PRODUCT="out/target/product/${name_product}"
    else
        DIR_PUB_SRC_PRODUCT="out/target/product/${TARGET_PRODUCT}"
    fi
    for f in ${PUBLISH_FILES}; do
        if [ "X${f}X" = "Xandroid-cts.zipX" ]; then
            f_src_path="out/host/linux-x86/cts/android-cts.zip"
        elif [ "X${f}X" = "Xandroid-vts.zipX" ]; then
            f_src_path="out/host/linux-x86/vts/android-vts.zip"
        elif [ "X${f}X" = "Xuserdebug_plat_sepolicy.cilX" ]; then
            f_src_path="${DIR_PUB_SRC_PRODUCT}/debug_ramdisk/${f}"
        else
            f_src_path="${DIR_PUB_SRC_PRODUCT}/${f}"
        fi


        echo "        \"${f_src_path}\"," >> "${f_tuxsuite_aosp}"
    done
    echo "        \"${DIR_PUB_SRC_PRODUCT}/build_fingerprint.txt\"" >> "${f_tuxsuite_aosp}"

    [ -z "${TUXSUITE_MANIFEST_URL}" ] &&  TUXSUITE_MANIFEST_URL="${MANIFEST_URL}"

cat >>"${f_tuxsuite_aosp}" <<__EOF__
    ],
    "sources": {
        "android": {
            "branch": "${MANIFEST_BRANCH}",
            "manifest": "${MANIFEST_FILENAME}",
            "url": "${TUXSUITE_MANIFEST_URL}",
            "patchset_project": "${TUXSUITE_PATCHSET_PROJECT}",
            "patchset_script": "${TUXSUITE_PATCHSET_SCRIPT}",
            "lunch_target": "${TARGET_PRODUCT}",
            "build_type": "userdebug"
        }
    },
    "targets": [
__EOF__

    targets_arr=()
    for target in ${MAKE_TARGETS}; do
        targets_arr+=("${target}")
    done
    targets_str=$(printf ",\"%s\"" "${targets_arr[@]}")
    targets_str=${targets_str:1}
    echo "        ${targets_str}" >> "${f_tuxsuite_aosp}"

    if [ -n "${CI_PIPELINE_ID}" ]; then
        tuxsuite_build_name="${CI_PIPELINE_ID}#${f_android_build_config_name}#$(date +%Y%m%d-%H%M%S)"
        TUXSUITE_ENVIRONMENTS="${TUXSUITE_ENVIRONMENTS} BUILD_NUMBER"
        # shellcheck disable=SC2034
        BUILD_NUMBER="${CI_PIPELINE_ID}"
    else
        tuxsuite_build_name="${f_android_build_config_name}#$(date +%Y%m%d-%H%M%S)"
    fi
cat >>"${f_tuxsuite_aosp}" <<__EOF__
    ],
__EOF__

    if [ -n "${TUXSUITE_ENVIRONMENTS}" ]; then
        echo "    \"environment\": {" >> "${f_tuxsuite_aosp}"
        env_arr=()
        for env  in ${TUXSUITE_ENVIRONMENTS}; do
            env_arr+=("\"${env}\": \"${!env}\"")
        done
        env_str=$(printf ",%s" "${env_arr[@]}")
        env_str=${env_str:1}
        echo "        ${env_str}" >> "${f_tuxsuite_aosp}"
        echo "    },"                 >> "${f_tuxsuite_aosp}"
    fi

cat >>"${f_tuxsuite_aosp}" <<__EOF__
    "container": "ubuntu-20.04",
    "name": "${tuxsuite_build_name}"
}
__EOF__

}

function generate_tuxsuite_aosp_local_manifest(){
    local f_tuxsuite_local="${1}"
    local f_android_build_config="${2}"

    # shellcheck disable=SC1090
    source "${f_android_build_config}"

    rm -fr "${f_tuxsuite_local}"
    if [ -n "${TUXSUITE_LOCAL_MANIFEST_FILE}" ]; then
        func_curl "${TUXSUITE_LOCAL_MANIFEST_FILE}" "${f_tuxsuite_local}"
    fi
}

############ COMMON FUNCTIONS ##########################

function main(){
    local name_android_build_config="${1}"

    local f_android_build_config="${dir_artifacts}/${name_android_build_config}"
    local f_tuxsuite_aosp="${dir_artifacts}/tuxsuite-aosp-${name_android_build_config}.json"
    local f_tuxsuite_aosp_local_manifest="${dir_artifacts}/tuxsuite-aosp-local-manifest-${name_android_build_config}.xml"
    local url_android_build_config="${url_prefix_configs}/${name_android_build_config}"

    opt_private=""
    ## when flag DEV_PRIVATE_GIT is set
    if [ -n "${DEV_PRIVATE_GIT}" ] && [ "${DEV_PRIVATE_GIT}" = "True" ]; then
        url_common_funcs_lkft="https://gitlab.com/Linaro/lkft/users/yongqin.liu/lkft-common/-/raw/development/lkft-common-bash-function.sh"
        f_common_funcs_lkft="scripts/lkft-common-bash-function.sh"
        func_curl "${url_common_funcs_lkft}" "${f_common_funcs_lkft}"
        # shellcheck source=/dev/null
        source "${f_common_funcs_lkft}"

        setup_lkft_bot_key
        git clone -b lkft "${REPO_ANDROID_BUILD_CONFIGS}" "${LKFT_WORK_DIR}/android-build-configs-private"
        clean_lkft_bot_key
        url_android_build_config="${LKFT_WORK_DIR}/android-build-configs-private/${name_android_build_config}"
        ## https://docs.tuxsuite.com/tuxbuild/additional-arguments/#private
        opt_private="--private"
        echo "TUXSUITE_GROUP=${TUXSUITE_GROUP}"
        export TUXSUITE_GROUP
    fi

    mkdir -p "${dir_artifacts}"
    rm -fr "${f_android_build_config}"
    func_curl "${url_android_build_config}" "${f_android_build_config}"

    generate_tuxsuite_aosp_configuration "${f_tuxsuite_aosp}" "${f_android_build_config}"
    generate_tuxsuite_aosp_local_manifest "${f_tuxsuite_aosp_local_manifest}" "${f_android_build_config}"

    local opt_local_manifest=""
    if [ -f "${f_tuxsuite_aosp_local_manifest}" ]; then
        opt_local_manifest="--local-manifest ${f_tuxsuite_aosp_local_manifest}"
    fi

    url_common_funcs="https://gitlab.com/Linaro/ltsk/ltsk-tuxtrigger/-/raw/main/scripts/ltsk-common-bash-function.sh"
    f_common_funcs="scripts/ltsk-common-bash-function.sh"
    func_curl "${url_common_funcs}" "${f_common_funcs}"
    # shellcheck source=/dev/null
    source "${f_common_funcs}"

    echo "Need to register Tuxsuite callback for the gitlab job"
    gitlab_callback_url=$(find_callback_job_url_from_pipeline "${CALLBACK_JOB_NAME}")
    echo "Callback URL: [${gitlab_callback_url}]"

    opt_callback_headers=""
    opt_callback_url=""
    if [ -n "${gitlab_callback_url}" ]; then
        opt_callback_url="--callback ${gitlab_callback_url}"
        if [ -n "${REGISTER_CALLBACK_TOKEN}" ]; then
            opt_callback_headers="--callback-header PRIVATE-TOKEN:${REGISTER_CALLBACK_TOKEN}"
        fi
    fi

    local f_tuxsuite_status="${dir_artifacts}/tuxsuite-aosp-output-${name_android_build_config}.json"
    local f_tuxsuite_log="${dir_artifacts}/tuxsuite-aosp-log-${name_android_build_config}.txt"
    # pip3 install --force-reinstall tuxsuite
    # shellcheck disable=SC2086
    tuxsuite bake submit "${f_tuxsuite_aosp}"  \
                            ${opt_local_manifest} \
                            ${opt_private} \
                            ${opt_callback_url} \
                            ${opt_callback_headers} \
                            --json-out "${f_tuxsuite_status}" \
                            --no-wait \
                            2>&1 | tee "${f_tuxsuite_log}"

}

############ COMMON FUNCTIONS ##########################
main "${name_android_build_config}"
